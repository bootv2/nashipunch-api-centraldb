const httpStatus = require("http-status");
const redis = require("../services/redisClient");
const httpMethods = require("../helpers/httpMethods");
const queryComposer = require("../helpers/queryComposer");

const { NODE_ENV } = require("../../config/server");

exports.verifyCache = (req, res, next) => {
  if (NODE_ENV !== "development" && req.method === httpMethods.get) {
    redis.get(composer.keyCache(req), (err, data) => {
      if (err) throw err;

      if (data != null) {
        const result = JSON.parse(data.toString());

        res.status(httpStatus.OK);
        res.json(result);
      } else {
        next();
      }
    });
  } else {
    next();
  }
};
