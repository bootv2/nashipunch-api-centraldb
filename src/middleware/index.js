const authMiddleware = require('./auth');
const redisMiddleware = require('./redisCache');

module.exports = {
  authMiddleware,
  redisMiddleware,
};
