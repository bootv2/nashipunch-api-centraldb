const postController = require("./postController");
const threadController = require("./threadController");
const authController = require("./authController");
const userController = require("./userController");

module.exports = {
  postController,
  threadController,
  authController,
  userController,
};