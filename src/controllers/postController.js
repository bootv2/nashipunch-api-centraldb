const httpStatus = require('http-status');
const datasource = require('../models/datasource');

const composer = require('../helpers/queryComposer')
const Response = require('../helpers/responseStrategy')
const Pagination = require('../helpers/PaginationBuilder')

const redis = require('../services/redisClient')

const { Post } = datasource().models;

exports.index = async (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  options.distinct = true;
  options.col = 'Post.id';

  try {    
    result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    Response.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.show = (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  scope
    .findOne(options)
    .then((result) => {
      redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

      Response.send(res, result, options);
    })
    .catch((err) => {
      console.error(err);
      Response.send(res, err, options);
    });
};
exports.store = async (req, res) => {
  const scope = composer.scope(req, Post);  

  try {
    const post = await Post.create({ ...req.body, user_id: req.user.id });

    const result = await scope.findOne({ where: { id: post.id } });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json(exception);
  }
};
exports.update = async (req, res) => {
  const scope = composer.scope(req, Post);

  try {
    const post = await Post.findOne({ where: req.params });

    const oldImage = post.image;

    await post.setTags(req.body.tags);
    const updated = await post.update(req.body);

    if (updated.image && updated.image.length > 0) {
      const image = await saveImage(updated.image);

      await sendImageToDelete(updated.image, oldImage);

      updated.image = image || updated.image;
      await updated.save();
    }

    const result = await scope.findOne({ where: req.params });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};
exports.destroy = async (req, res) => {
  try {
    const post = await Post.findOne({ where: req.params });
    const postImage = JSON.parse(post.image);

    await deleteImage(postImage);
  } catch (err) {
    console.log('Error deleting image: ', err);
  }

  Post.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Post.count(options)
    .then((result) => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch((err) => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};