const GoogleAuth = require("google-auth-library");
const httpStatus = require("http-status");
const jwt = require("jwt-simple");
const datasource = require("../models/datasource");

const config = require("../../config/server");

const auth = new GoogleAuth();
const { GOOGLE_CLIENT_ID, JWT_SECRET } = config;

const { User } = datasource().models;
const userController = require('./userController');

const clientId = GOOGLE_CLIENT_ID;
const secret = JWT_SECRET;

module.exports = {
  googleLogin: (req, res) => {

    if (req.headers.gtoken) {
      const client = new auth.OAuth2(clientId, "", "");

      // Check if it's a valid Google Auth token
      console.log('--------------')
      console.log("Check if it's a valid Google Auth token")
      console.log('--------------')

      client.verifyIdToken(req.headers.gtoken, clientId, async (error, login) => {
        if (error) {
          console.log(error);
          console.log('Could not verify that token. Check authController client.verifyIdToken!')
          res.sendStatus(httpStatus.UNAUTHORIZED);
          return;
        }
        console.log('OK.');

        let payload;

        // Try to get the payload from that login
        console.log('--------------')
        console.log("Try to get the payload from that login")
        console.log('--------------')

        try {
          payload = login.getPayload();
          console.log('OK.');
        } catch (err) {
          console.log(err);
          res.sendStatus(httpStatus.UNAUTHORIZED);
          return;
        }

        // Try to get the details of that user using that email
        console.log('--------------')
        console.log("Try to get the details of that user using that email")
        console.log('--------------')

        try {
          const result = await User.findOne({
            where: {
              email: payload.email
            }
          });
          
          // If no user exists, create a new one:
            if(!result) {
              console.log('NO USER FOUND WITH THAT EMAIL. CREATING USER.');
              try {
                const result = await userController.store({
                  ...req,
                  body: {
                    email: payload.email,
                    external_type: 'GOOGLE',
                    external_id: payload.sub,
                    usergroup: 0,
                  }
                }, res);  

                const { username, avatar_url, usergroup } = result;
                const responseFields = { username, avatar_url, usergroup, ttl: Date.now() };
    
                res.status(httpStatus.OK);
                res.json({
                  token: jwt.encode(result, secret),
                  ...responseFields,
                });
    
                console.log('USER CREATED.')
                return;
              } catch (err) {
                console.log(err);
                res.sendStatus(httpStatus.UNAUTHORIZED);
                return;
              }
            }
          
          // If a user exists, get its data:
            console.log('USER FOUND.')
            const { username, avatar_url, usergroup } = result.dataValues;
            const responseFields = { username, avatar_url, usergroup, ttl: Date.now() };
    
            res.status(httpStatus.OK);
            res.json({
              token: jwt.encode(result, secret),
              ...responseFields,       
            });

            console.log('AUTH OPERATION COMPLETE.');
          } catch (err) {
            console.log(err);
            res.sendStatus(httpStatus.UNAUTHORIZED);
          }
  })
}
}};
