const httpStatus = require('http-status');
const datasource = require('../models/datasource');

const composer = require('../helpers/queryComposer')
const Response = require('../helpers/responseStrategy')
const Pagination = require('../helpers/PaginationBuilder')

const redis = require('../services/redisClient')

const { User } = datasource().models;

exports.store = async (req, res) => {    
  try {
    const user = await User.create(req.body);

    const scope = composer.scope(req, User); 
    const options = composer.options(req, User.blockedFields);

    const resultingUser = scope.findOne({...options, where: {id: user.id}});

    return resultingUser
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json(exception);
  }
};
exports.update = async (req, res) => {
  const scope = composer.scope(req, User);

  try {    
    const user = await User.findOne({ where: req.user });

    const updated = await user.update(req.body);

    const result = await scope.findOne({ where: req.user });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};