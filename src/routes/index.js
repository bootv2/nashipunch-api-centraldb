const express = require('express');

const errorHandler = require('../services/errorHandler');
const { redisMiddleware, authMiddleware } = require('../middleware');
const {
  postController,
  threadController,
  authController,
  userController,
} = require('../controllers');

const catchErrors = errorHandler.catchErrors;
const authentication = authMiddleware.authentication;
const router = express.Router();

router.use(redisMiddleware.verifyCache);

// Auth
router.get('/auth/google', authController.googleLogin);

// User
router.put('/user', authentication, userController.update);

// Posts
router.get('/post/count', postController.count);

router.get('/post', authentication, catchErrors(postController.index));
router.get('/post/:id', postController.show);
router.post('/post', authentication, catchErrors(postController.store));
router.put('/post/:id', authentication, catchErrors(postController.update));
router.delete('/post/:id', postController.destroy);

// Threads
router.get('/thread', catchErrors(threadController.index));
router.get('/thread/:id', catchErrors(threadController.show));
router.post('/thread', authentication, catchErrors(threadController.store));

module.exports = router;