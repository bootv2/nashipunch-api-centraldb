const Response = require("../helpers/responseStrategy");

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/
exports.catchErrors = fn => {
  return function(req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

// Always remember to exec this after all routes on the app
exports.catchHandler = app => {
  app.use((req, res, next) => {
    Response.send(res, null, null);
  });

  app.use((err, req, res, next) => {
    console.error(err);
    Response.send(res, err, null);
  });
};