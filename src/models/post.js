"use strict";
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define(
    "Post",
    {
      content: DataTypes.STRING,
      user_id: DataTypes.UUID,
      thread_id: DataTypes.UUID,
      createdAt: {type: DataTypes.DATE, field: 'created_at'},
      updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
    }, {
      timestamps: true,
    }
  );
  Post.associate = function(models) {
    // A post belongs to a user
    Post.User = Post.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
    // A post can belong to a thread
    Post.Thread = Post.belongsTo(models.Thread, { foreignKey: 'id' });
  };
  Post.initScopes = () => {
    Post.addScope('defaultScope', {
      include: [
        {
          association: Post.User,
          attributes: ['username', 'usergroup', 'avatar_url', 'created_at'],
        },
      ],
    }, {override: true,});
  }
  return Post;
};
