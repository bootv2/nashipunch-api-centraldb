const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");

const datasource = require("./src/models/datasource");
const routes = require("./src/routes");
const { APP_PORT } = require("./config/server");
const errorHandler = require("./src/services/errorHandler");

const app = express();
app.datasource = datasource();

app.use(helmet());
app.use(compression());

app.use(
  cors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    exposedHeaders: "Content-Range,X-Content-Range,X-Total-Count"
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);

errorHandler.catchHandler(app);

app.listen(APP_PORT, () => console.log('🚀 App listening on ', APP_PORT))

module.exports = app;