# nashipunch-api
### a NodeJS forum backend

# license

right now, there is no official license. assume that you can't reuse this code for other things, that you're willingly contributing to this project and that you're giving away your contributions and that nothing belongs to you etc.  
i'll figure out the best license for it later, at which point your contributions should gain the (more permissive) rights of that license.

# running this project

requirements:

* Node v9 or later
* MySQL server (legacy auth might be required)
* Redis
    * Windows: https://github.com/ServiceStack/redis-windows/raw/master/downloads/redis-latest.zip  
    Run `redis-server.exe` when running the API.
    * Linux: https://redis.io/topics/quickstart
    * OSX: https://medium.com/@djamaldg/install-use-redis-on-macos-sierra-432ab426640e

recommended:

* yarn

instructions:

1. clone the repo
2. navigate to `/nashipunch-api`
3. run a `yarn` (or `npm install` if you promise you won't push npm-related files to the repo)
4. start up redis and your mysql server (in case you hadn't)
5. run `yarn global add sequelize-cli` (or `npm install -g sequelize-cli`) to install the sequelize CLI
5. run `sequelize db:create nashipunch_db` to create the database
6. run `sequelize db:migrate` to create the tables
7. run `yarn start` (or `npm start`) to fire up your server
8. open `http://localhost:3000/thread` on your browser to test that the API is working

# contributing to nashipunch-api

1. fork this repository
2. do work, push it to your repository
3. open **your** repository's bitbucket page, hit the + button on the sidebar
4. `Create a pull request`
5. target `nashipunch/nashipunch-api`, fill in the title and description fields, create the PR!
6. @ Inacio on Facepunch (if it still exists) or in the Favela Lounge Discord server and let me know you created a PR so i can give it a look

comments, suggestions, additions and criticism* are always welcome. thanks for your help!

*unless that criticism is about this project being made with javascript